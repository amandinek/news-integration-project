from django.db import models

# Create your models here.
class News(models.Model):
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    author = models.TextField(blank=True, null=True)
    image=models.ImageField(blank=True, null=True)
    MEDIA_HOUSE = (('bbc', 'BBC'),
                   ('aljazeera', 'Al Jazeera'),
                   # ('aljazeera', 'Al Jazeera'),
                   )
    provider = models.CharField(max_length=255, choices=MEDIA_HOUSE, blank=True, null=True)
    # pusbishedAt = models.CharField(max_length=255)

    class Meta:
        ordering = ['-id']

    def __str__(self):
        return self.title