from django.shortcuts import render
from django.db.models import Q

import os
from newsapi import NewsApiClient
# import json
from .models import News
# import psycopg2
# from psycopg2.extras import execute_values


# Create your views here.



def Home(request, *args, **kwargs):

    # # get serch word
    # search = request.GET.get('search')
    #
    # if search: # user is searching
    #     news = News.objects.filter(Q(title__icontains=search) | Q(description__icontains=search) )
    # else: # landing page
    #     news  = News.objects.all()[:50]

    return render(request, "index.html", {})


def Index(request):
    # article= News()
    newsapi = NewsApiClient(api_key="1b65d1e946504034a6bf74c18d1fde1f")
    headLines = newsapi.get_top_headlines(country="gb", category="technology")
    articles = headLines["articles"]
    # newss = json.loads(articles.read())

    news = []
    description = []
    url = []
    image = []
    author = []
    published = []
    for i in range(len(articles)):
        newArtical = articles[i]
        news.append(newArtical["title"])
        description.append(newArtical["description"])
        url.append(newArtical["url"])
        image.append(newArtical["urlToImage"])
        author.append(newArtical["author"])
        published.append(newArtical["publishedAt"])

    myList = zip(news, description, url, image, author, published)
        # save
    article = News(title=newArtical["title"],
                       description=newArtical["description"],
                       provider="BBC",
                       author=newArtical["author"],
                       image=newArtical["urlToImage"])
    article.save()






    print("article saved")
    # print(myList)
    return render(request, "bbc.html", context={"myList": myList})


def AlNews(request):
    newsapi = NewsApiClient(api_key="1b65d1e946504034a6bf74c18d1fde1f")
    headLines = newsapi.get_top_headlines(country="us", category="business")
    articles = headLines["articles"]

    news = []
    description = []
    url = []
    image = []
    author = []
    published = []

    for i in range(len(articles)):
        newArtical = articles[i]
        news.append(newArtical["title"])
        description.append(newArtical["description"])
        url.append(newArtical["url"])
        image.append(newArtical["urlToImage"])
        author.append(newArtical["author"])
        published.append(newArtical["publishedAt"])

        article = News(title=newArtical["title"],
                           description=newArtical["description"],
                           provider="aljazeera",
                           author=newArtical["author"],
                           image=newArtical["urlToImage"])
    article.save()

    mynews = zip(news, description, url, image, author, published)
    print("news")


    return render(request, "alJasira.html", context={"mynews": mynews})

def Searchbar(request):
    search = request.GET.get('search')

    if search:  # user is searching
        news = News.objects.filter(Q(title__icontains=search) | Q(description__icontains=search))
        print("11111111111111111111111")
    else:  # landing page
        print("biteeeeeeeeeeeeeeee")
        news = News.objects.all()[:50]
        print("2222222222222222")
    return render(request, "searchbar.html", context={"news":news})


