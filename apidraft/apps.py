from django.apps import AppConfig


class ApidraftConfig(AppConfig):
    name = 'apidraft'
